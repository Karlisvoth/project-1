<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKategoriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama', 45);
            $table->timestamps();
        });

            // Insert some stuff
        DB::table('kategori')->insert(
            array(
                ['nama' => 'Makanan'],
                ['nama' => 'Elekronik'],
                ['nama' => 'Kecantikan'],
                ['nama' => 'Fashion'],
                ['nama' => 'Hobi'],
                ['nama' => 'Lainnya']
                )
            );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori');
    }
}
