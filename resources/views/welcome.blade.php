@extends('main.master')

@section('judul2')
    <h3>Welcome!</h3>
@endsection

@section('content')
<div class="col">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          {{-- <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> --}}
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="img/new-product-2.jpeg" class="d-block w-100" alt="...">
          </div>
          <!-- <div class="carousel-item">
            <img src="img/Screen-Shot-2015-01-19-at-12.38.19-PM-500x337.png" class="d-block w-100" alt="...">
          </div> -->
          <div class="carousel-item">
            <img src="img/PROMO-3.jpg" class="d-block w-100" alt="...">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <div class="row my-3">
            <div class="col">
                <div class="card" style="width: auto;">
                    <img src="{{asset('img/barang-1.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <div class="container-fluid">
                            <ul class="nav justify-content-between">
                                <li class="card-title"><a class="text-dark text-decoration-none" href="/barang">Cari Barang</a></li>
                            {{-- <p class="card-text">10 pieces of chicken legs for sharing and party. Value pack!!!</p> --}}
                                <li class="nav-item">
                                    <a href="/barang" class="btn btn-info">Telusuri</a>
                                </li>
                            </ul>
                    </div>
                    </div>
                  </div>
            </div>

            @guest
            <div class="col">
                <div class="card" style="width: auto;">
                    <img src="{{asset('img/signup-0.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <div class="container-fluid">
                            <ul class="nav justify-content-between">
                                <li class="card-title"><a class="text-dark text-decoration-none" href="/register">Sign-Up!</a></li>
                            {{-- <p class="card-text">10 pieces of chicken legs for sharing and party. Value pack!!!</p> --}}
                                <li class="nav-item">
                                    <a href="/register" class="btn btn-info">Buat Akun</a>
                                </li>
                            </ul>
                    </div>
                    </div>
                  </div>
            </div>
            @endguest

            @auth
            <div class="col">
                <div class="card" style="width: auto;">
                    <img src="{{asset('img/addsign.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <div class="container-fluid">
                            <ul class="nav justify-content-between">
                                <li class="card-title"><a class="text-dark text-decoration-none" href="/barang/create">Tambah Barang</a></li>
                            {{-- <p class="card-text">10 pieces of chicken legs for sharing and party. Value pack!!!</p> --}}
                                <li class="nav-item">
                                    <a href="/barang/create" class="btn btn-info">Input Item</a>
                                </li>
                            </ul>
                    </div>
                    </div>
                  </div>
            </div>    
            @endauth

            <div class="col">
                <div class="card" style="width: auto;">
                    <img src="{{asset('img/contact-1.jpg')}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <div class="container-fluid">
                            <ul class="nav justify-content-between">
                                <li class="card-title"><a class="text-dark text-decoration-none" href="contact">Call Center</a></li>
                            {{-- <p class="card-text">10 pieces of chicken legs for sharing and party. Value pack!!!</p> --}}
                                <li class="nav-item">
                                    <a href="/contact" class="btn btn-info">Hubungi</a>
                                </li>
                            </ul>
                    </div>
                    </div>
                  </div>
            </div>
        </div>
    </div>

@endsection