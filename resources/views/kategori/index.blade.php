@extends('main.master')

@section('judul2')
    <h3>Tambah Kategori</h3>
@endsection

@section('content')
<a href="/kategori/create" class="btn btn-primary mb-3">Tambah Kategori</a>
<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Kategori</th>
        <th scope="col">Tindakan</th>
        </tr>
    </thead>
    <tbody>
     @forelse ($kategori as $key => $item)
     <tr>
         <td>{{$key + 1}}</td>
         <td>{{$item->nama}}</td>
         <td>
            <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm"><i class="fas fa-file-alt"></i></a>                 
            <a href="/kategori/{{$item->id}}/edit" class="btn btn-success btn-sm"><i class="fas fa-edit"></i></a>
            <td>
            <form class="mt-2" action="/kategori/{{$item->id}}" method="POST">
                @csrf
                @method('delete')
                <button type="submit" value="delete" class="btn btn-danger btn-sm">
            <i class="fas fa-trash-alt"></i></button>
            </form>
          </td>
        </td>
     </tr>
         
    @empty
    <h1>Kategori Kosong</h1>
     @endforelse
    </tbody>
  </table>


@endsection