@extends('main.master')

@section('judul2')
    <h3>Tambah Edit Kategori</h3>
@endsection

@section('content')

<form method="POST" action="/kategori/{{$kategori->id}}">
    @csrf
    @method('put')
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama" value="{{$kategori->nama}}" class="form-control"><br>
      @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
       
    <button type="submit" class="btn btn-primary">Update</button>
  </form>


@endsection