@extends('main.master')

@section('judul2')
    <h3>Tambah Kategori</h3>
@endsection

@section('content')

<form method="POST" action="/kategori">
    @csrf
    <div class="form-group">
      <label>Nama Kategori</label>
      <input type="text" name="nama" class="form-control"><br>
      @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
       
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection