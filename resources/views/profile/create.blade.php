@extends('main.master')

@section('judul2')
    <h3>Create Profile</h3>
@endsection

@section('content')


      {{-- <p class="login-box-msg">Create aprifl</p> --}}

      <form action="/profile" method="post">
        @csrf
        <div class="form-group">
            <label>Profile Picture :</label><br>
            <input type="file" name="pro_pic">
            @error('pro_pic')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        @error('email')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        @error('password')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
        
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="row d-flex justify-content-center m-2">
          <a href="/login" class="text-decoration-none">I already have a membership</a>
         </div>
    
    
    
            <div class="row">
                <button type="submit" class="btn btn-primary btn-block">Register</button>
              </div>
    
      </form>


@endsection