@extends('main.master')

@section('judul2')
    <h3>Edit Barang</h3>
@endsection

@push('script')
    <script>
        tinymce.init({
            selector: '#desc_edit'
        });
    </script>
@endpush

@section('content')
    <form action="/barang/{{$barang->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama :</label>
        <input type="text" name="nama" class="form-control" value="{{$barang->nama}}">
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Harga :</label>
        <input type="number" name="harga" class="form-control" value="{{$barang->harga}}" min='0' placeholder="satuan rupiah">
        @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Kategori :</label><br>
        <select name="kategori_id" class="js-example-basic-single" style="width: 100%;">
            <option value="">---Pilih Kategori---</option>
            @foreach ($kategori as $item)
                @if ($item->id === $barang->kategori_id)
                    
                <option value="{{$item->id}}" selected>{{$item->nama}}</option>

                @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Thumbnail :</label>
        <input type="file" name="thumbnail" value="{{$barang->thumbnail}}" class="form-control">
        @error('thumbnail')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gambar 2 :</label>
        <input type="file" name="image2" value="{{$barang->image2}}" class="form-control">
        {{-- @error('image2')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}
    </div>
    <div class="form-group">
        <label>Gambar 3 :</label>
        <input type="file" name="image3" value="{{$barang->image3}}" class="form-control">
        {{-- @error('image3')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror --}}
    </div>
    <div class="form-group">
        <label>Stok :</label>
        <input type="number" name="stok" class="form-control" value="{{$barang->stok}}" min='0' placeholder="jumlah barang yang ada">
        @error('stok')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Deskripsi :</label>
        <textarea name="deskripsi" class="form-control" id="desc_edit" cols="30" rows="10">{{$barang->deskripsi}}</textarea>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
        <input type="submit" value="Submit">
    </form>
@endsection