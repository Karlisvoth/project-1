@extends('main.master')
@section('judul2')
    <h3>Tambah Barang</h3>
@endsection

{{-- @push('style')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />       
@endpush --}}

@push('script')
    {{-- <script src='https://cdn.tiny.cloud/1/6gl7sun9kghs0laqh763d0tt0pk31ss5ei2h32o4jzpla7ui/tinymce/5/tinymce.min.js' referrerpolicy="origin">
        </script> --}}
    <script>
        tinymce.init({
            selector: '#desc'
        });
</script>
    
@endpush

@section('content')
@auth
    <form action="/barang" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label>Nama :</label><br>
        <input type="text" name="nama" class="form-control">
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Harga :</label><br>
        <input type="number" name="harga" class="form-control" min='0' placeholder="satuan rupiah">
        @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Kategori :</label><br>
        <select name="kategori_id" class="js-example-basic-single" style="width:100%;">
            <option value="" style="width:100%; height:auto;">---Pilih Kategori---</option>
            @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label>Thumbnail :</label><br>
        <input type="file" name="thumbnail" class="form-control">
        @error('thumbnail')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Gambar 2 :</label><br>
        <input type="file" name="image2" class="form-control">
        @error('image2')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
    </div>
    <div class="form-group">
        <label>Gambar 3 :</label><br>
        <input type="file" name="image3" class="form-control">
        @error('image3')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
    </div>
    <div class="form-group">
        <label>Stok :</label><br>
        <input type="number" class="form-control" name="stok" min='0' placeholder="jumlah barang yang ada">
        @error('stok')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label>Deskripsi :</label><br>
        <textarea name="deskripsi" cols="30" rows="10" id=desc></textarea>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
        <input type="submit" value="Submit" class="btn btn-primary">
    </form>
@endauth

<a href="/barang" class="btn btn-outline-danger my-3"><i class="mdi mdi-close"></i></a>

</div>
@endsection

{{-- @push('script')
<script>
    $(document).ready(function() {
    $('.js-example-basic-single').select2();
    });
</script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
@endpush --}}