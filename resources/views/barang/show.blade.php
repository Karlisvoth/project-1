@extends('main.master')

@section('judul2')
    <h2>{{$barang->nama}}</h2>
@endsection
@push('script')
<script>
  tinymce.init({
      selector: '#desc_show'
  });
</script>

@endpush
@section('content')
<div class="row">
<div class="col-4">
    <div id="carouselExampleIndicators" class="carousel slide" data-interval="false">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="{{asset('img_upload/'.$barang->thumbnail)}}" style="maxwidth: 100px; maxheight: 100px;" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="{{asset('img_upload/'.$barang->image2)}}" class="d-block w-100" alt="...">
          </div>
          <div class="carousel-item">
            <img src="{{asset('img_upload/'.$barang->image3)}}" class="d-block w-100" alt="...">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

    <div class="col-2">
      <h3>Harga</h3>
      <h3>Stok</h3>
      <h3>Kategori</h3>
      <form action="/barang" method="POST">
        @csrf
        <div class="btn btn-outline-info">
        <input type="submit" name="barang_id" value="Buy Now" class="btn"><br>
        </div>
      </form>
        <a href="#" class="btn btn-outline-info my-1"><i class="fas fa-cart-plus"></i></a><br>
      <form action="/barang/{{$barang->id}}" method="POST">
        @csrf
        @method('DELETE')
      @auth
        <a href="/barang/{{$barang->id}}/edit" class="btn btn-outline-warning my-1"><i class="fas fa-edit"></i></a><br>
        <span class="btn btn-outline-danger my-1">
        <i class="fas fa-trash-alt"></i><input type="submit" class="btn" value="Delete">
      @endauth
        </span>
      </form>
    </div>
    <div class="col">
      <h3>: Rp. {{$barang->harga}}</h3>
      <h3>: {{$barang->stok}}</h3>
      @foreach ($kategori as $kat)
      @if ($kat->id === $barang->kategori_id)
      <h3 value="{{$kat->id}}" selected>: {{$kat->nama}}</h3>
      @else
      @endif
      @endforeach
    </div>
</div>
<div class="row my-2">
  <h3>Deskripsi:</h3>
</div>
<div class="row">
  <textarea cols="100" rows="20" disabled>{{$barang->deskripsi}}</textarea>
</div>

@foreach($barang->ulasan as $ulas)
<div class="row my-2">
  <h3>Ulasan:</h3>
</div>
<div class="row">
  <div class="col-3">
    <div class="card" style="maxwidth: 300px; maxheight: 300px;">
      <div class="card-body p-3">
        <h4> {{$ulas->profile->nickname}}</h4><hr>
        <h5 class="card-text">{{$ulas->isi}}</h5>
  {{-- <textarea cols="auto" rows="auto" disabled>{{$ulas->isi}}</textarea> --}}
      </div>
    </div>
  </div>
</div>
@endforeach

<form action="/ulasan" method="POST" enctype="multipart/form-data">
@csrf
  <div class="form-group my-3">
  <label>Ulasan :</label><br>
  <input type="hidden" name="barang_id" value="{{$barang->id}}">
  <textarea name="isi" cols="30" rows="10" class="form-control"></textarea>
  @error('isi')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
</div>
  <input type="submit" value="Submit" class="btn btn-primary">
</form>

@endsection