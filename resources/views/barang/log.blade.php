@extends('main.master')

@section('judul2')
    <h3>Purchase History</h3>
@endsection

@section('content')
    @foreach ($barang->transaksi as $item)
        <h3>{{$item->profile->nickname}} telah membeli {{$item->jumlah}} buah {{$item->barang->nama}}</h3>
        <br><br>
    @endforeach

@endsection