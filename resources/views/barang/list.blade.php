@extends('main.master')

@section('judul2')
    <h3>Barang</h3>
@endsection

@section('content')
@auth
<a href="/barang/create" class="btn btn-primary sm-3"><i class="fas fa-plus-circle"></i>&nbsp;&nbsp;Tambah Barang</a>
@endauth
<div class="row my-3">
    @forelse($barang as $item)
    <div class="col-3">
        <div class="card" style="maxwidth: 300px; maxheight: 300px;">
            <img style="" src="{{asset('img_upload/'.$item->thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body p-3">
              <h5 class="card-title">{{Str::limit($item->nama, 20)}}</h5>
              <h5 class="card-text">Rp. {{$item->harga}}</h5>
              <a href="#" class="btn btn-outline-info m-1"><i class="fas fa-cart-plus"></i></a>
              <a href="#" class="btn btn-outline-info m-1">Buy Now</a>
              <a href="/barang/{{$item->id}}" class="btn btn-outline-info"><i class="fas fa-file-alt"></i></a>
            @auth
                <a href="/barang/{{$item->id}}/edit" class="btn btn-outline-warning m-1"><i class="fas fa-edit"></i></a>
                <form action="/barang/{{$item->id}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <span class="btn btn-outline-danger my-1">
                  <i class="fas fa-trash-alt"></i><input type="submit" class="btn text-muted" value="Delete">
                  </span>
                </form>
            @endauth
            </div>
          </div>
    </div>
    @empty

    <h3>No Data</h3>
  @endforelse
</div>
@endsection