<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="text-center sidebar-brand-wrapper d-flex align-items-center">
      <a class="sidebar-brand brand-logo" href="#"><img src="{{asset('Breeze/template/assets/images/logotypes.png')}}" alt="logo" /></a>
      <a class="sidebar-brand brand-logo-mini pl-4 pt-4" href="/"><img src="{{asset('Breeze/template/assets/images/mini-logo.png')}}" alt="logo" /></a>
    </div><br>
    <ul class="nav">


          @auth
          <li class="nav-item nav-profile">
          <a href="#" class="nav-link">
          <div class="nav-profile-image">
            <img src="{{asset('img/img_206976.png')}}" alt="profile" />
            <span class="login-status online"></span>
            <!--change to offline or busy as needed-->
          </div>

          <div class="nav-profile-text d-flex flex-column pr-3">
            <span class="font-weight-medium mb-2">User</span>            
          </div>
          
          </a>
          </li>
          @endauth


      <li class="nav-item">
        <a class="nav-link" href="/">
          <i class="mdi mdi-home menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      @auth
               
      <li class="nav-item">
        <a class="nav-link" href="/kategori">
          <i class="mdi mdi-format-list-bulleted menu-icon"></i>
          <span class="menu-title">Kategori</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/barang">
          <i class="mdi mdi-table-large menu-icon"></i>
          <span class="menu-title">Produk/Barang</span>
        </a>
      </li>
      
      <li class="nav-item sidebar-actions">
        <div class="nav-link">
          <div class="mt-4">
            {{-- <div class="border-none">
              <p class="text-black">Notification</p>
            </div> --}}           
          </div>
        </div>
      </li>
      @endauth
      @guest
      <li class="nav-item">
      <a class="nav-link" id="login" href="/login">
        <i class="fa fa-sign-in menu-icon" aria-hidden="true"></i> 
        <span class="menu-title">Login </span></a>
      </li>
    @endguest
    </ul>
  </nav>
