<div id="theme-settings" class="settings-panel">
    <i class="settings-close mdi mdi-close"></i>
    <p class="settings-heading">SIDEBAR SKINS</p>
    <div class="sidebar-bg-options selected" id="sidebar-default-theme">
      <div class="img-ss rounded-circle bg-light border mr-3"></div> Default
    </div>
    <div class="sidebar-bg-options" id="sidebar-dark-theme">
      <div class="img-ss rounded-circle bg-dark border mr-3"></div> Dark
    </div>
    <p class="settings-heading mt-2">HEADER SKINS</p>
    <div class="color-tiles mx-0 px-4">
      <div class="tiles light"></div>
      <div class="tiles dark"></div>
    </div>
  </div>
  <nav class="navbar col-lg-12 col-12 p-lg-0 fixed-top d-flex flex-row">
    <div class="navbar-menu-wrapper d-flex align-items-stretch justify-content-between">
      <a class="navbar-brand brand-logo-mini align-self-center d-lg-none" href="index.html"><img src="{{asset('Breeze/template/assets/images/logo-mini.svg')}}" alt="logo" /></a>
      <button class="navbar-toggler navbar-toggler align-self-center mr-2" type="button" data-toggle="minimize">
        <i class="mdi mdi-menu"></i>
      </button>
    <ul class="navbar-nav">
      <li class="nav-item nav-search border-0 ml-1 ml-md-3 ml-lg-5 d-none d-md-flex">
        <form class="nav-link form-inline mt-2 mt-md-0">
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="mdi mdi-magnify"></i>
              </span>
            </div>
          </div>
        </form>
      </li>
    </ul>

      <ul class="navbar-nav navbar-nav-right ml-lg-auto">
        
        @guest
          <li class="nav-item d-none d-xl-flex border-0">
          <a class="nav-link" id="login" href="/login">
            <i class="fa fa-sign-in mr-2" aria-hidden="true"></i> Login </a>
          </li>
        @endguest

        @auth
        <li class="nav-item d-none d-xl-flex border-0 btn btn-primary">
          <a class="nav-link" href="">
            <i class="fas fa-shopping-cart"></i>
            <span class="menu-title">Keranjang</span>
          </a>
        </li>


          <li class="nav-item nav-profile dropdown border-0">
            <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown">
              <img class="nav-profile-img mr-2" alt="" src="{{asset('img/img_206976.png')}}" />
              <span class="profile-name">User</span>
            </a>
            <div class="dropdown-menu navbar-dropdown w-100" aria-labelledby="profileDropdown">
              {{-- <a class="dropdown-item" href="#">
                <i class="mdi mdi-cached mr-2 text-success"></i> Activity Log </a> --}}
              
                @auth
                
                
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();"><i class="mdi mdi-logout mr-2 text-primary"></i>
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              
            
            @endauth
                
            </div>
          </li>
        @endauth
      </ul>
      <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
      </button>
    </div>
  </nav>
