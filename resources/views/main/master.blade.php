<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Project 1</title>
    <link rel="stylesheet" href="{{asset('Breeze/template/assets/vendors/mdi/css/materialdesignicons.min.css')}}" />
    <link rel="stylesheet" href="{{asset('Breeze/template/assets/vendors/flag-icon-css/css/flag-icon.min.css')}}" />
    <link rel="stylesheet" href="{{asset('Breeze/template/assets/vendors/css/vendor.bundle.base.css')}}" />
    <link rel="stylesheet" href="{{asset('Breeze/template/assets/vendors/font-awesome/css/font-awesome.min.css')}}" />
    <link rel="stylesheet" href="{{asset('Breeze/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" />
    <link rel="stylesheet" href="{{asset('Breeze/template/assets/css/style.css')}}" />
    <link rel="shortcut icon" href="{{asset('Breeze/template/assets/images/favicon.png')}}" />
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.15.4/css/all.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />       
    @stack ('style')
  </head>
  <body>
    <div class="container-scroller">

    @include('sweetalert::alert')

    @include('partial.navbar')

    @include('partial.sidebar')


      <div class="container-fluid page-body-wrapper">
        <div class="main-panel">
          <div class="content-wrapper p-5">

            <div>
              <section class="content-header">
                <div class="container-fluid">
                  <div class="row mb-2">
                    <div class="col-sm-6">
                      <h1>@yield('judul')</h1>
                    </div>
                  </div>
                </div><!-- /.container-fluid -->
              </section>
          
              <!-- Main content -->
              <section class="content">
          
                <!-- Default box -->
                <div class="card">
                  <div class="card-header">
                    <h3 class="card-title">@yield('judul2')</h3>
          
                  </div>
                  <div class="card-body">
                    @yield('content')
      
                  </div>
                  <!-- /.card-body -->
                  <!-- /.card-footer-->
                </div>
                <!-- /.card -->
          
              </section>          
            </div>
          </div>
          <footer class="footer">
            <div class="d-sm-flex justify-content-center justify-content-sm-between">
              <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © bootstrapdash.com 2020</span>
              <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"> Free <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap dashboard template</a> from Bootstrapdash.com</span>
            </div>
          </footer>
        </div>
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{asset('Breeze/template/assets/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="https://kit.fontawesome.com/e9a941d891.js" crossorigin="anonymous"></script>
    <script src="{{asset('Breeze/template/assets/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/vendors/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/vendors/flot/jquery.flot.resize.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/vendors/flot/jquery.flot.categories.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/vendors/flot/jquery.flot.fillbetween.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/vendors/flot/jquery.flot.stack.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/vendors/flot/jquery.flot.pie.js')}}"></script>
    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('Breeze/template/assets/js/off-canvas.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('Breeze/template/assets/js/misc.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
    <script src="{{asset('Breeze/template/assets/js/dashboard.js')}}"></script>
    <!-- End custom js for this page -->
    <script>
      $(document).ready(function() {
      $('.js-example-basic-single').select2();
      });
    </script>
      <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
      <script src='https://cdn.tiny.cloud/1/6gl7sun9kghs0laqh763d0tt0pk31ss5ei2h32o4jzpla7ui/tinymce/5/tinymce.min.js' referrerpolicy="origin"></script>

    @stack('script')
  </body>
</html>