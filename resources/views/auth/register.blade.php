@extends('main.master')

@section('judul2')
    <h3>Sign-Up Form</h3>
@endsection

@section('content')


      <p class="login-box-msg">Register a new membership</p>

      <form action="{{ route('register') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="input-group mb-3">
          <input type="text" name="name" class="form-control" placeholder="Full name">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        @error('name')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        @error('email')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        @error('password')
        <div class="alert alert-danger">{{$message}}</div>
        @enderror
        
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password_confirmation" placeholder="Retype password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>

        <div class="form-group">
          <label>Profile Picture :</label><br>
          <input type="file" name="propic" class="form-control">
        </div>
          @error('propic')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          
          <div class="input-group mb-3">
            <input type="text" name="nickname" class="form-control" placeholder="Nickname">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-user"></span>
              </div>
            </div>
          </div>
          @error('nickname')
          <div class="alert alert-danger">{{$message}}</div>
          @enderror
  

        <div class="form-group">
          <label>Umur :</label><br>
          <input type="number" class="form-control" name="umur" min='0' placeholder="Umur">
        </div>
          @error('umur')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror

        <div class="form-group">
          <label>Alamat :</label><br>
          <textarea name="alamat" cols="30" rows="10" id=address></textarea>
        </div>
          @error('alamat')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror  
  
        <div class="row d-flex justify-content-center m-2">
          <a href="/login" class="text-decoration-none">I already have a membership</a>
         </div>
    
    
    
            <div class="row">
                <button type="submit" class="btn btn-primary btn-block">Register</button>
              </div>
    
      </form>


@endsection