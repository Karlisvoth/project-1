@extends('main.master')

@section('judul2')
    <h3>Sign-Up Form</h3>
@endsection

@section('content')
    
<form action="{{ route('register') }}" method="post">
    @csrf
    <div class="input-group mb-3">
      <input type="text" name="name" class="form-control" placeholder="Full name">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-user"></span>
        </div>
      </div>
    </div>
    @error('name')
      <div class="alert alert-warning">
        {{ $message }}
      </div>
    @enderror

    <div class="input-group mb-3">
      <input type="email" name="email" class="form-control" placeholder="Email">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-envelope"></span>
        </div>
      </div>
    </div>
    @error('email')
    <div class="alert alert-warning">
      {{ $message }}
    </div>
    @enderror

    <div class="input-group mb-3">
      <input type="password" name="password" class="form-control" placeholder="Password">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-lock"></span>
        </div>
      </div>
    </div>
    @error('password')
    <div class="alert alert-warning">
      {{ $message }}
    </div>
    @enderror

    <div class="input-group mb-3">
      <input type="password" name="password_confirmation" class="form-control" placeholder="Retype password">
      <div class="input-group-append">
        <div class="input-group-text">
          <span class="fas fa-lock"></span>
        </div>
      </div>
    </div>
    {{-- @error('password_confirmation')
    <div class="alert alert-warning">
      {{ $message }}
    </div>
    @enderror --}}
    <div class="input-group mb-3">
        <input type="text" name="nickname" class="form-control" placeholder="Profile name">
    </div>
      @error('nickname')
    <div class="alert alert-warning">
      {{ $message }}
    </div>
    @enderror

    <div class="input-group mb-3">
        <input type="number" name="umur" class="form-control" placeholder="Umur" min="0">
    </div>
    @error('umur')
    <div class="alert alert-warning">
      {{ $message }}
    </div>
    @enderror

    <div class="input-group mb-3">
        <textarea name="alamat" id="alamat" class="form-control" placeholder="Alamat"></textarea>
    </div>
    @error('alamat')
    <div class="alert alert-warning">
      {{ $message }}
    </div>
    @enderror

    <div class="row">
      {{-- <div class="col-8">
        <div class="icheck-primary">
          <input type="checkbox" id="agreeTerms" name="terms" value="agree">
          <label for="agreeTerms">
           I agree to the <a href="#">terms</a>
          </label>
        </div>
      </div> --}}
      <!-- /.col -->
      <div class="col">
        <button type="submit" class="btn btn-primary btn-block">
            {{ __('Register') }}
        </button>
      </div>
      <!-- /.col -->
    </div>
  </form>
@endsection
