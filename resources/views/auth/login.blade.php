@extends('main.master')

@section('judul2')
    <h3>Login</h3>
@endsection

@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body"> --}}
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col offset-md-4">
                                <div class="form-check">
                                    <label class="form-check-label" for="remember">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                                {{-- <div class="form-check form-check-flat form-check-primary">
                                    <label class="form-check-label">
                                      <input type="checkbox" class="form-check-input" /> Remember me </label>
                                  </div>             --}}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col offset-md-4">
                                <button type="submit" class="btn btn-primary mr-3">
                                    {{ __('Login') }}
                                </button>
                                <span>or</span>
                                {{-- <button type="button" class="btn btn-primary ml-3"> --}}
                                    <a href="/register" class="ml-3">Sign-Up!</a>
                                {{-- </button> --}}

                                
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col offset-md-4">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link mt-3" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                    @endif
                                </div>
                            </div>

                        {{-- <div>
                            <p class="text-center">Test</p>
                        </div> --}}
                    </form>
                {{-- </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
