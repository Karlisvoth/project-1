<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/testing', 'IndexController@testing');

// CRUD Barang
Route::group(['middleware' => ['auth']], function () {
    Route::resource('ulasan', 'UlasanController')->only([
        'index', 'store'
    ]);

    // Create
    Route::get('/barang/create', 'BarangController@create'); //munuju form create Barang
    Route::post('/barang', 'BarangController@store'); //mengambil data
    // Create
    Route::get('/barang/create', 'BarangController@create'); //munuju form create Barang
    Route::post('/barang', 'BarangController@store'); //mengambil data
    // Update
    Route::get('/barang/{barang_id}/edit', 'BarangController@edit'); //mengubah suatu data
    Route::put('/barang/{barang_id}', 'BarangController@update'); //memasukkan perubahan data
});

// Read
Route::get('/barang', 'BarangController@index'); //melihat table Barang
Route::get('/barang/{barang_id}', 'BarangController@show'); //manmpilkan data dengan id tertentu


// Delete
Route::delete('/barang/{barang_id}', 'BarangController@destroy'); //menghapus data


// CRUD KATEGORI

//Create
Route::get('/kategori/create', 'KategoriController@create'); //munuju form create Kategori
Route::post('/kategori', 'KategoriController@store'); //mengambil data kategori

//Read
Route::get('/kategori', 'KategoriController@index'); //melihat table Kategori
Route::get('/kategori/{kategori_id}', 'KategoriController@show'); //manmpilkan data dengan id tertentu

// Update
Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit'); //mengubah suatu data
Route::put('/kategori/{kategori_id}', 'KategoriController@update'); //memasukkan perubahan data

// Delete
Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy'); //menghapus data



// CRUD PROFILE
//Create
Route::get('/profile/create', 'UsersController@create'); //munuju form create Profile
Route::post('/profile', 'UsersController@store'); //mengambil data

// Read
Route::get('/profile', 'UsersController@index'); //melihat table Profile
Route::get('/profile/{profile_id}', 'UsersController@show'); //manmpilkan data dengan id tertentu

// Update
Route::get('/profile/{profile_id}/edit', 'UsersController@edit'); //mengubah suatu data
Route::put('/profile/{profile_id}', 'UsersController@update'); //memasukkan perubahan data

// Delete
Route::delete('/profile/{profile_id}', 'UsersController@destroy'); //menghapus data

//AUTHENTICATION
Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');