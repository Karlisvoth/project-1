<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    public function kategori(){
        return $this->belongsTo('App\Kategori');
      }
    //   public function profile()
    //   {
    //       return $this->belongsToMany('App\Profile', 'Transaksi'); //, 'profile_id', 'barang_id');
    //   }
    public function ulasan(){
        return $this->hasMany('App\Ulasan');
    }

    public function transaksi(){
        return $this->hasMany('App\Transaksi');
    }

    protected $table = "barang";

    protected $fillable = ["nama", "harga", "thumbnail", "image2", "image3", "stok", "deskripsi", "kategori_id"];
}
