<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
    public function barang(){
        return $this->belongsTo('App\Barang');
      }

    public function profile(){
        return $this->belongsTo('App\Profile');
      }

    protected $table = "ulasan";

    protected $fillable = ["isi", "profile_id", "barang_id"];

}
