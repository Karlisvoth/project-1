<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;

class ProfileController extends Controller
{
    public function index(){
        $profile = Profile::where('users_id', Auth::id())->first();

        return view('profile.index', compact('profile'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'propic' => 'required',
            'nickname' => 'required',
            'umur' => 'required',
            'alamat' => 'required',
        ]);
        $profile = Profile::find($id);

        $profile->propic = $request['propic'];
        $profile->nickname = $request['nickname'];
        $profile->umur = $request['umur'];
        $profile->alamat = $request['alamat'];

        $profile->save();

        return redirect('/');
    }
}
