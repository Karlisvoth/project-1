<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Profile;
use Image;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'propic' => ['required', 'image', 'mimes:jpeg,png,jpg,svg', 'max:2048'],
            'nickname' => ['required', 'string', 'max:255'],
            'umur' => ['required', 'integer', 'max:255'],
            'alamat' => ['required', 'string', 'max:1024'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {   
        $request = app('request');
        if ($request->hasfile('propic')) {
            $propic = $request->file('propic');
            $filename = time() . '.' . $propic->getClientOriginalExtension();
    
    
            Image::make($propic)->resize(300, 300)->save(public_path('pfp/' . $filename));
        }

        $users = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);


        Profile::create([
            'propic' => !empty($filename) ? $filename : 'default_avatar.png',
            'nickname' => $data['nickname'],
            'umur' => $data['umur'],
            'alamat' => $data['alamat'],
            'users_id' => $users->id
        ]);


        return $users;
    }
}
