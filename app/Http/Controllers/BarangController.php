<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use DB;
use RealRashid\SweetAlert\Facades\Alert;

class BarangController extends Controller
{
    public function index() {
        $barang = Barang::all();
        return view('barang.list', compact('barang'));
    }

    public function create() {
        $kategori = DB::table('kategori')->get();
        return view('barang.create', compact('kategori'));
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'image2' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'image3' => 'required|image|mimes:jpeg,png,jpg,svg|max:2048',
            'stok' => 'required',
            'deskripsi' => 'required',
            'kategori_id' => 'required'
        ]);

        $thumbnailName = time().'.'.$request->thumbnail->extension();
        $image2Name = time().'_2.'.$request->image2->extension();
        $image3Name = time().'_3.'.$request->image3->extension();

        $request->thumbnail->move(public_path('img_upload'), $thumbnailName);
        $request->image2->move(public_path('img_upload'), $image2Name);
        $request->image3->move(public_path('img_upload'), $image3Name);

        $barang = new Barang;
 
        $barang->nama = $request->nama;
        $barang->harga = $request->harga;
        $barang->thumbnail = $thumbnailName;
        $barang->image2 = $image2Name;
        $barang->image3 = $image3Name;
        $barang->stok = $request->stok;
        $barang->deskripsi = $request->deskripsi;
        $barang->kategori_id = $request->kategori_id;
        $barang->save();

        Alert::success('Success', 'Item Dimasukkan');

        return redirect('/barang');
        // dd($request->all());
    }

    public function show($id) {
        $kategori = DB::table('kategori')->get();
        $barang = Barang::findOrFail($id);
        return view('barang.show', compact('barang', 'kategori'));
    }

    public function edit($id) {
        $kategori = DB::table('kategori')->get();
        $barang = Barang::findOrFail($id);
        return view('barang.edit', compact('barang', 'kategori'));
    }

    public function update(Request $request, $id) {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'thumbnail' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
            'image2' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
            'image3' => 'image|mimes:jpeg,png,jpg,svg|max:2048',
            'stok' => 'required',
            'deskripsi' => 'required',
            'kategori_id' => 'required'
        ]);

        $barang = Barang::find($id);

        if ($request->has('thumbnail' || 'image2' || 'image3')){
            $thumbnailName = time().'.'.$request->thumbnail->extension();
            $image2Name = time().'_2.'.$request->image2->extension();
            $image3Name = time().'_3.'.$request->image3->extension();
    
            $request->thumbnail->move(public_path('img_upload'), $thumbnailName);
            $request->image2->move(public_path('img_upload'), $image2Name);
            $request->image3->move(public_path('img_upload'), $image3Name);
         
            $barang->nama = $request->nama;
            $barang->harga = $request->harga;
            $barang->thumbnail = $thumbnailName;
            $barang->image2 = $image2Name;
            $barang->image3 = $image3Name;
            $barang->stok = $request->stok;
            $barang->deskripsi = $request->deskripsi;
            $barang->kategori_id = $request->kategori_id;    
        } else {
            $barang->nama = $request->nama;
            $barang->harga = $request->harga;
            $barang->stok = $request->stok;
            $barang->deskripsi = $request->deskripsi;
            $barang->kategori_id = $request->kategori_id;    
        }
 
        $barang->update();

        Alert::success('Success', 'Update Berhasil');

        return redirect('/barang');
    }

    public function destroy($id) {
        $barang = Barang::find($id);

        $barang->delete();

        Alert::success('Success', 'Item Terhapus');

        return redirect('/barang');
    }

}
