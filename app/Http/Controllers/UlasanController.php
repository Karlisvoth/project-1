<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Ulasan;

class UlasanController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'isi' => 'required'
        ]);

    $ulasan = new Ulasan;

    $ulasan->isi = $request->isi;
    $ulasan->profile_id = Auth::id();
    $ulasan->barang_id = $request->barang_id;

    $ulasan->save();

    return redirect()->back();
    }

}
