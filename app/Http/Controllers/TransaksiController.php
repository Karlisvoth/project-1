<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use App\Barang;
use App\Transaksi;

class TransaksiController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'jumlah' => 'required'
        ]);

    $transaksi = new Transaksi;

    $transaksi->jumlah = $request->jumlah;
    $transaksi->profile_id = Auth::id();
    $transaksi->barang_id = $request->barang_id;

    $transaksi->save();

    return redirect('/barang/log');
    }
}
