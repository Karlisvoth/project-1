<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
      }
    //   public function barang()
    //   {
    //       return $this->belongsToMany('App\Barang', 'Transaksi', 'profile_id', 'barang_id')->withPivot('jumlah');
    //   }
    public function ulasan(){
        return $this->hasMany('App\Ulasan');
    }

    public function transaksi(){
        return $this->hasMany('App\Transaksi');
    }
  
    protected $table = "profile";

    protected $fillable = ["propic","nickname", "umur", "alamat", "users_id"];
}
