<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    public function barang(){
        return $this->belongsTo('App\Barang');
      }
    public function profile(){
        return $this->belongsTo('App\Profile');
      }
    protected $table = "transaksi";

    protected $fillable = ["jumlah", "profile_id", "barang_id"];
}
