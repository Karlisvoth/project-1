<h1>Kelompok 7 Grup 1 Batch 2</h1>

 <table>
  <tr>
    <th>Nama Anggota</th>
    <th>Username Gitlab</th>
    <th>Username Telegram</th>
  </tr>
  <tr>
    <td>Udin Nurwachid</td>
    <td>udinkintaro29</td>
    <td>UdinKintaro</td>
  </tr>
  <tr>
    <td>Alfa Abdullah</td>
    <td>Karlisvoth</td>
    <td>Karlisvoth</td>
  </tr>
    <tr>
    <td>Raden Ayu Dela Ulva Sa'adah</td>
    <td>delaulva</td>
    <td>Dela Ulva Sa'adah</td>
  </tr>
</table> 

<h2>Tema Projek: E-Commerce</h2>

<img src="public/img/ERD.png">

<h2>Template yang digunakan: <a href="https://themewagon.com/themes/breeze-free-bootstrap-4-responsive-admin-dashboard-template/">Breeze</a></h2>

<h2>Demo Video, <a href="https://youtu.be/1X4pIgpK_h8">here</a></h2>
